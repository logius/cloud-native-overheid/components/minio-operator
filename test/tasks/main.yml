- name: Test namespace
  set_fact: 
    namespace_name: "test-{{ 9999999999999999999999 | random | to_uuid }}"

- name: Create random namespace
  k8s:
    name: "{{ namespace_name }}"
    api_version: v1
    kind: Namespace
    state: present

- name: Set OpenShift security context constraint
  include: test/tasks/scc-test.yml
  when: config.system.k8s_distribution == 'openshift'

- name: Tenant credentials
  k8s:
    state: present
    namespace: "{{ namespace_name }}"
    definition:
      apiVersion: v1
      kind: Secret
      metadata:
        name: minio-creds-secret
      type: Opaque
      data:
        ## Access Key for MinIO Tenant, base64 encoded (echo -n 'minio' | base64)
        accesskey: bWluaW8=
        ## Secret Key for MinIO Tenant, base64 encoded (echo -n 'minio123' | base64)
        secretkey: bWluaW8xMjM=

- name: Install network policy for operator
  k8s:
    state: present
    definition:
      kind: NetworkPolicy
      apiVersion: networking.k8s.io/v1
      metadata:
        name: allow-minio-operator
        namespace: "{{ namespace_name }}"
      spec:
        ingress:
        - from:
          - namespaceSelector:
              matchLabels:
                name: "{{ config.minio_operator.namespace }}"
        podSelector: {}
        policyTypes:
        - Ingress

- name: Create a minio instance
  k8s:
    state: present
    namespace: "{{ namespace_name }}"
    definition:
      apiVersion: minio.min.io/v2
      kind: Tenant
      metadata:
        name: minio
      labels:
        app: minio
      spec:
        ## Registry location and Tag to download MinIO Server image
        image: "{{ minio_repository | default('minio') }}/minio:latest"
        imagePullPolicy: IfNotPresent

        ## Secret with credentials to be used by MinIO Tenant.
        ## Refers to the secret object created above.
        credsSecret:
          name: minio-creds-secret

        ## Enable automatic Kubernetes based certificate generation and signing as explained in
        ## https://kubernetes.io/docs/tasks/tls/managing-tls-in-a-cluster        
        requestAutoCert: false

        ## Specification for MinIO Pool(s) in this Tenant.
        pools:
            ## Servers specifies the number of MinIO Tenant Pods / Servers in this pool.
            ## For standalone mode, supply 1. For distributed mode, supply 4 or more.
            ## Note that the operator does not support upgrading from standalone to distributed mode.
          - servers: 4

            ## volumesPerServer specifies the number of volumes attached per MinIO Tenant Pod / Server.
            volumesPerServer: 1

            ## This VolumeClaimTemplate is used across all the volumes provisioned for MinIO Tenant in this
            ## Pool.
            volumeClaimTemplate:
              metadata:
                name: data
              spec:
                accessModes:
                  - ReadWriteOnce
                resources:
                  requests:
                    storage: 100Gi

            ## Configure resource requests and limits for MinIO containers
            resources:
              requests:
                cpu: 250m
                memory: 4Gi
              limits:
                cpu: 500m
                memory: 4Gi

            ## Configure security context 
            securityContext:
              runAsUser: 1000
              runAsGroup: 1000
              runAsNonRoot: true

- name: Wait for cluster
  k8s_info:
    api_version: minio.min.io/v2
    kind: Tenant
  register: tenant
  retries: 100
  delay: 10
  until: tenant.resources[0].status.pools | default([]) | json_query("[?state=='PoolInitialized' || type=='Failed']")
  failed_when: tenant.resources[0].status.pools | default([]) | json_query("[?state=='Failed']")

- name: Remove test namespace
  k8s:
    name: "{{ namespace_name }}"
    api_version: v1
    kind: Namespace
    state: absent
    wait: true
  