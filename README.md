# minio-operator

Installs the MinIO operator.

Links:
- Chart sources and flags: https://github.com/minio/operator/blob/master/helm/operator/values.yaml
- Releases: https://github.com/minio/operator/releases
- Docs: https://docs.min.io/minio/k8s/

# Test Tenant

The Ansible test tag installs a simple MinIO cluster with 4 MinIO servers with each 1 Persistent Volume.